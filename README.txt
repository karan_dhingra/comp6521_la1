1. Generate dummy data : 

	Execute "DummyData" class in package "DummyDataGenerator"

2. Data:
	Place data files with names "data1.txt" and "data2.txt" in data folder

3. Set memory limit:
 	Right click on LA1MAIN class
 	Select "Run Configurations" - This should bring up a dialog box.
 	In the left pane of the dialog box, select "LA1MAIN"  under "Java Application" tab. Then click on the Argument tab.
 	Fill "VM arguments" section with : -Xmx20M (to set memory limit of 20 MB)
 									 : -Xmx10M (to set memory limit of 10 MB)

4. Run LA1MAIN as Java Application