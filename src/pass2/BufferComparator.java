package pass2;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

public class BufferComparator implements Comparator<Buffer> {

	@Override
	public int compare(Buffer o1, Buffer o2) {
		try {
			if (o1.getTopElement() > o2.getTopElement()) {
				return 1;
			} else if (o1.getTopElement() < o2.getTopElement()) {
				return -1;
			} else if (o1.getTopElement() != Integer.MAX_VALUE && o2.getTopElement() != Integer.MAX_VALUE
					&& o1.getTopElement() == o2.getTopElement()) {
				Date o1RecordDate = new SimpleDateFormat("yyyy-MM-dd")
						.parse(new String(o1.peek(), StandardCharsets.UTF_8).substring(9, 19));
				Date o2RecordDate = new SimpleDateFormat("yyyy-MM-dd")
						.parse(new String(o2.peek(), StandardCharsets.UTF_8).substring(9, 19));
				if (o1RecordDate.before(o2RecordDate)) {
					return 1;
				} else {
					return -1;
				}
			}
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
