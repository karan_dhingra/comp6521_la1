package pass2;

import java.io.BufferedReader;
import java.io.IOException;

import appConstants.AppConstants;

public class Buffer {

	private BufferedReader br = null;
	private int topId;
	private int currentPointer;
	private int[] empIds = null;
	private byte[][] data = null;

	public Buffer(BufferedReader br) throws IOException {
		this.br = br;
		this.currentPointer = 0;
		this.empIds = new int[AppConstants.blockSize];
		this.data = new byte[AppConstants.blockSize][];
		fillBuffer();
	}

	private void fillBuffer() throws IOException {
		String line;
		int count = 0;
		while (count < AppConstants.blockSize && (line = br.readLine()) != null) {
			int empId = Integer.parseInt(line.substring(0, 8));
			empIds[count] = empId;
			data[count] = line.getBytes();
			count++;
		}
		
		if(count > 0) {
			AppConstants.pass2InputCount = AppConstants.pass2InputCount + 1;
		}
		
		while (count < AppConstants.blockSize) {
			empIds[count] = Integer.MAX_VALUE;
			data[count] = null;
			count++;
		}

		this.topId = empIds[0];
		this.currentPointer = 0;

	}

	public int getTopElement() throws IOException {
		return this.topId;
	}

	public byte[] poll() throws IOException {

		byte[] result = data[this.currentPointer];
		if (this.currentPointer == AppConstants.blockSize - 1) {
			fillBuffer();
		} else {
			this.currentPointer++;
			this.topId = this.empIds[this.currentPointer];
		}
		return result;
	}

	public byte[] peek() throws IOException {
		return data[this.currentPointer];
	}

}
