package pass2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Comparator;
import java.util.PriorityQueue;

import appConstants.AppConstants;
import memory.Memory;

public class Pass2 {

	public static void executePhase2(int numberOfChunks) throws IOException {
		Memory memory = Memory.getInstance();
		int numberOfBuffers = memory.numberOfBlocksInAChunk;
		int numberOfInputBuffers = numberOfBuffers - 1;
		if (numberOfInputBuffers <= numberOfChunks) {
			System.err.println("ERROR: Number of Chunks exceeded number of available buffers");
			return;
		}
		int numberOfInputBuffersPerChunk = numberOfInputBuffers / numberOfChunks;

		Comparator<Buffer> comparator = new BufferComparator();
		PriorityQueue<Buffer> inputBuffers = new PriorityQueue<Buffer>(comparator);

		instantiateInputBuffers(inputBuffers, numberOfChunks, numberOfInputBuffersPerChunk);

		mergeData(inputBuffers);

	}

	private static void mergeData(PriorityQueue<Buffer> inputBuffers) throws IOException {

		byte[] data = new byte[100];
		int outputBufferCapacity = AppConstants.blockSize;
		String mergeFilePath = AppConstants.mergedDataFile;

		BufferedWriter writer = new BufferedWriter(new FileWriter(mergeFilePath, false));

		int lastFlushedId = -1;

		while (!inputBuffers.isEmpty()) {

			if (outputBufferCapacity == 0) {
				AppConstants.pass2OutputCount = AppConstants.pass2OutputCount + 1;
				outputBufferCapacity = AppConstants.blockSize;
				writer.flush();
			}

			Buffer buffer = inputBuffers.poll();

			int topId = buffer.getTopElement();
			if (topId == Integer.MAX_VALUE) {
				continue;
			}

			data = buffer.poll();
			if (data == null) {
				break;
			}

			inputBuffers.add(buffer);

			if (lastFlushedId != topId) {
				writer.append(new String(data, StandardCharsets.UTF_8) + "\n");
				lastFlushedId = topId;
				outputBufferCapacity--;
			}

		}

		AppConstants.pass2OutputCount = AppConstants.pass2OutputCount + 1;

		writer.close();
	}

	private static void instantiateInputBuffers(PriorityQueue<Buffer> inputBuffers, int numberOfChunks,
			int numberOfInputBuffersPerChunk) throws IOException {
		for (int i = 1; i <= numberOfChunks; i++) {
			BufferedReader br = new BufferedReader(
					new FileReader(AppConstants.sortedChunksPath + "chunk" + i + ".txt"));
			for (int j = 0; j < numberOfInputBuffersPerChunk; j++) {
				Buffer buffer = new Buffer(br);
				inputBuffers.add(buffer);
			}
		}
	}

}
