package appConstants;

public class AppConstants {

	public static String dataFile1 = "./data/data1.txt";
	public static String dataFile2 = "./data/data2.txt";
	public static String sampleFile = "./data/sample.txt";
	public static String sortedChunksPath = "./data/chunks/";
	public static String mergedDataFile = "./data/mergedData.txt";

	public static int pass1InputCount = 0;
	public static int pass1OutputCount = 0;
	
	public static int pass2InputCount = 0;
	public static int pass2OutputCount = 0;
	
	public static int blockSize = 40;

}
