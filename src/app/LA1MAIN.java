package app;

import java.io.IOException;
import java.text.ParseException;

import appConstants.AppConstants;
import pass1.Pass1;
import pass2.Pass2;

public class LA1MAIN {

	public static void main(String args[]) {
		double start = (double) System.currentTimeMillis();
		double phase2EndTime = 0;
		double phase1EndTime = 0;

		try {
			
			System.out.println("############## Pass1 ##############");
			Pass1.executePhase1();
			phase1EndTime = (double) System.currentTimeMillis();
			System.out.println("Pass 1 Execution time :" + (phase1EndTime - start) / 1000 + " seconds");
			System.out.println("#Inputs: " + AppConstants.pass1InputCount);
			System.out.println("#Outputs: " + AppConstants.pass1OutputCount);
			
			System.out.println("\n############## Pass2 ##############");
			Pass2.executePhase2(Pass1.chunksCreated);
			phase2EndTime = (double) System.currentTimeMillis();
			System.out.println("Pass 2 Execution time :" + (phase2EndTime - phase1EndTime) / 1000 + " seconds");
			System.out.println("#Inputs: " + AppConstants.pass2InputCount);
			System.out.println("#Outputs: " + AppConstants.pass2OutputCount);

			System.out.println("\nExecution time :" + (phase2EndTime - start) / 1000 + " seconds");
			System.out.println("Total Inputs: " + (AppConstants.pass1InputCount + AppConstants.pass2InputCount));
			System.out.println("Total Outputs: " + (AppConstants.pass1OutputCount + AppConstants.pass2OutputCount));

		} catch (NumberFormatException | IOException | ParseException e) {
			e.printStackTrace();
		}

	}

}
