package pass1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Arrays;

import appConstants.AppConstants;
import memory.Memory;

//TODO: Execute without calling garbage collection
public class Pass1 {

	public static int chunksCreated = 0;

	public static void executePhase1() throws NumberFormatException, IOException, ParseException {

		Memory memory = Memory.getInstance();

		int numberOfBlocksInAChunk = memory.numberOfBlocksInAChunk;
		int numberOfRecordsInAChunk = numberOfBlocksInAChunk * AppConstants.blockSize;

		readAndSortData(AppConstants.dataFile1, numberOfRecordsInAChunk);
		readAndSortData(AppConstants.dataFile2, numberOfRecordsInAChunk);

	}

	private static void readAndSortData(String filePath, int numberOfRecordsInAChunk)
			throws NumberFormatException, IOException, ParseException {

		int[] empIds = new int[numberOfRecordsInAChunk];
		byte[][] data = new byte[numberOfRecordsInAChunk][];

		BufferedReader br = new BufferedReader(new FileReader(filePath));

		String line;
		int index = 0;

		while ((line = br.readLine()) != null) {
			int empId = Integer.parseInt(line.substring(0, 8));
			empIds[index] = empId;
			data[index] = line.substring(8).getBytes();
			index++;
			if (index == numberOfRecordsInAChunk) {
				AppConstants.pass1InputCount = AppConstants.pass1InputCount + (numberOfRecordsInAChunk/AppConstants.blockSize);
				MergeSort.mergeSort(empIds, data, 0, numberOfRecordsInAChunk - 1);
				saveChunkIntoDisk(empIds, data, numberOfRecordsInAChunk);
				empIds = null;
				data = null;
				empIds = new int[numberOfRecordsInAChunk];
				data = new byte[numberOfRecordsInAChunk][];
				// System.gc();
				index = 0;
			}
		}

		if (index != 0) {
			AppConstants.pass1InputCount = AppConstants.pass1InputCount + (index/AppConstants.blockSize);
			MergeSort.mergeSort(empIds, data, 0, index - 1);
			saveChunkIntoDisk(empIds, data, index);
			empIds = null;
			data = null;
			empIds = new int[numberOfRecordsInAChunk];
			data = new byte[numberOfRecordsInAChunk][];
		}

		// System.gc();

		br.close();

	}

	private static void saveChunkIntoDisk(int[] empIds, byte[][] data, int length) throws IOException {

		chunksCreated++;
		String chunkFilePath = AppConstants.sortedChunksPath + "chunk" + chunksCreated + ".txt";
		BufferedWriter writer = new BufferedWriter(new FileWriter(chunkFilePath, false));
		for (int i = 0; i < length; i++) {
			if (empIds[i] == 0) {
				break;
			}
			writer.append(empIds[i] + new String(data[i], StandardCharsets.UTF_8) + "\n");

		}
		
		AppConstants.pass1OutputCount = AppConstants.pass1OutputCount + (length/AppConstants.blockSize);
		
		writer.close();
	}

}
