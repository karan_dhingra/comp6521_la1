package pass1;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class MergeSort {

	public static void mergeSort(int[] empIds, byte[][] data, int left, int right) throws ParseException {
		if (left < right) {
			int middle = (left + right) / 2;

			mergeSort(empIds, data, left, middle);
			mergeSort(empIds, data, middle + 1, right);

			merge(empIds, data, left, middle, right);
		}
	}

	private static void merge(int[] empIds, byte[][] data, int left, int middle, int right) throws ParseException {
		int n1 = middle - left + 1;
		int n2 = right - middle;

		int empIdsL[] = new int[n1];
		int empIdsR[] = new int[n2];
//		String[] dataL = new String[n1];
//		String[] dataR = new String[n2];
		byte[][] dataL = new byte[n1][];
		byte[][] dataR = new byte[n2][];

		for (int i = 0; i < n1; ++i) {
			empIdsL[i] = empIds[left + i];
			dataL[i] = data[left + i];
		}

		for (int j = 0; j < n2; ++j) {
			empIdsR[j] = empIds[middle + 1 + j];
			dataR[j] = data[middle + 1 + j];
		}

		int i = 0, j = 0;

		int k = left;
		while (i < n1 && j < n2) {
			if (empIdsL[i] <= empIdsR[j]) {
				empIds[k] = empIdsL[i];
				mergeDataSubListInASortedManner(empIds, data, dataL, k, i, left);
				// data[k] = dataL[i];
				i++;
			} else {
				empIds[k] = empIdsR[j];
				mergeDataSubListInASortedManner(empIds, data, dataR, k, j, left);
				// data[k] = dataR[j];
				j++;
			}
			k++;
		}

		while (i < n1) {
			empIds[k] = empIdsL[i];
			// data[k] = dataL[i];
			mergeDataSubListInASortedManner(empIds, data, dataL, k, i, left);
			i++;
			k++;
		}

		while (j < n2) {
			empIds[k] = empIdsR[j];
			// data[k] = dataR[j];
			mergeDataSubListInASortedManner(empIds, data, dataR, k, j, left);
			j++;
			k++;
		}
	}

	private static void mergeDataSubListInASortedManner(int[] empIds, byte[][] data, byte[][] dataSubList, int k, int i,
			int left) throws ParseException {

		if (k == left || (k > left && empIds[k - 1] != empIds[k])) {
			data[k] = dataSubList[i];
			return;
		}

		Date recordDate = new SimpleDateFormat("yyyy-MM-dd")
				.parse(new String(dataSubList[i], StandardCharsets.UTF_8).substring(0, 10));

		while (k > left && empIds[k - 1] == empIds[k]) {

			String previousRecord = new String(data[k - 1], StandardCharsets.UTF_8);
			Date previousRecordDate = new SimpleDateFormat("yyyy-MM-dd").parse(previousRecord.substring(0, 10));
			if (previousRecordDate.after(recordDate)) {
				break;
			} else {
				data[k] = data[k - 1];
			}

			k--;

		}
		data[k] = dataSubList[i];

	}

}
